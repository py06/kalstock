### DISCLAIMER
### This is an example Makefile and it MUST be configured to suit your needs.
### For detailed explanations about all of the available options, please refer
### to https://github.com/sudar/Arduino-Makefile/blob/master/arduino-mk-vars.md
### Original project where this Makefile comes from: https://github.com/WeAreLeka/Bare-Arduino-Project

### PROJECT_DIR
### This is the path to where you have created/cloned your project
ARDUINO_BASE	= ${HOME}/development/arduino
ARDUINO_SKETCHBOOK = ${ARDUINO_BASE}/sketches
PROJECT_DIR       = ${ARDUINO_SKETCHBOOK}/kalstock

### ARDMK_DIR
### Path to the Arduino-Makefile directory.
ARDMK_DIR         = ${ARDUINO_BASE}/Arduino-Makefile

### ARDUINO_DIR
### Path to the Arduino application and resources directory.
### On OS X:
#ARDUINO_DIR       = /Applications/Arduino.app/Contents/Java
### or on Linux: (remove the one you don't want)
ARDUINO_SYS	 = /home/peewhy/development/arduino/Arduino/build/linux/work
ARDUINO_DIR       = ${ARDUINO_SYS}
BOARDS_TXT	= ${ARDUINO_DIR}/hardware/arduino/avr/boards.txt

### ARDUINO_LIB_PATH
### Path to where the arduino main libraries.
ARDUINO_LIB_PATH = $(ARDUINO_DIR)/libraries

### USER_LIB_PATH
### Path to where the your project's libraries are stored.
USER_LIB_PATH    :=  $(ARDUINO_BASE)/sketchbook/libraries

### BOARD_TAG
### It must be set to the board you are currently using. (i.e uno, mega2560, etc.)
VARIANT	=	standard
BOARD_TAG         = uno
MCU = atmega328p
F_CPU = 16000000L

### MONITOR_BAUDRATE
### It must be set to Serial baudrate value you are using.
MONITOR_BAUDRATE  = 115200

### AVR_TOOLS_DIR
### Path to the AVR tools directory such as avr-gcc, avr-g++, etc.
### On OS X with `homebrew`:
#AVR_TOOLS_DIR     = /usr/local
### or on Linux: (remove the one you don't want)
AVR_TOOLS_DIR     = /usr

### AVRDUDE
### Path to avrdude directory.
### On OS X with `homebrew`:
#AVRDUDE          = /usr/local/bin/avrdude
### or on Linux: (remove the one you don't want)
AVRDUDE          = /usr/bin/avrdude

### CFLAGS_STD
### Set the C standard to be used during compilation. Documentation (https://github.com/WeAreLeka/Arduino-Makefile/blob/std-flags/arduino-mk-vars.md#cflags_std)
CFLAGS_STD        += -std=gnu11

### CXXFLAGS_STD
### Set the C++ standard to be used during compilation. Documentation (https://github.com/WeAreLeka/Arduino-Makefile/blob/std-flags/arduino-mk-vars.md#cxxflags_std)
CXXFLAGS_STD      += -std=gnu++11

### CXXFLAGS
### Flags you might want to set for debugging purpose. Comment to stop.
CXXFLAGS         += -pedantic -Wall -Wextra

### MONITOR_PORT
### The port your board is connected to. Using an '*' tries all the ports and finds the right one.
MONITOR_PORT      = /dev/ttyUSB*

### CURRENT_DIR
### Do not touch - used for binaries path
CURRENT_DIR       = $(shell basename $(CURDIR))

### OBJDIR
### This is where you put the binaries you just compile using 'make'
OBJDIR            = $(PROJECT_DIR)/build_$(BOARD_TAG)/$(CURRENT_DIR)

ARDUINO_CORE_PATH = ${ARDUINO_DIR}/hardware/arduino/avr/cores/arduino
ARDUINO_VAR_PATH = ${ARDUINO_DIR}/hardware/arduino/avr/variants
ARDUINO_LIBS = SPI MD_MAX72XX MD_Parola OneWire DS18B20
ARCHITECTURE = avr
AVRDUDE_CONF = /etc/avrdude.conf
AVRDUDE_ARD_BAUDRATE = 57600

### Do not touch - the path to Arduino.mk, inside the ARDMK_DIR
include $(ARDMK_DIR)/Arduino.mk

