#include <MD_MAX72xx.h>
#include <SPI.h>
#include <OneWire.h>
#include <DS18B20.h>

/* 1-Wire bus Arduino pin */
const byte ONEWIRE_PIN = 9;

/* Sensor address
EXAMPLE:
byte sensorAddress[8] = {0x28, 0xB1, 0x6D, 0xA1, 0x3, 0x0, 0x0, 0x11};
*/
byte sensorAddress[8];

float curTemp = 0.0;
OneWire onewire(ONEWIRE_PIN);
DS18B20 sensors(&onewire);

#define USE_POT_CONTROL 1
#define PRINT_CALLBACK  0

#define PRINT(s, v) { Serial.print(F(s)); Serial.print(v); }

/* Define the number of devices we have in the chain and the hardware interface
NOTE: These pin numbers will probably not work with your hardware and may
need to be adapted */
#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES 8

#define CLK_PIN   13  // or SCK
#define DATA_PIN  11  // or MOSI
#define CS_PIN    10  // or SS

/* SPI hardware interface */
MD_MAX72XX mx = MD_MAX72XX(HARDWARE_TYPE, CS_PIN, MAX_DEVICES);

/* Scrolling parameters */
#if USE_POT_CONTROL
#define SPEED_IN  A5
#else
#define SCROLL_DELAY  75  /* in milliseconds */
#endif /* USE_POT_CONTROL */

#define DS18B20_POLL_PERIOD	(5*1000) /* 5 seconds */
enum state_e {
	SENS_IDLE = 0,
	SENS_REQ = 1
};
enum state_e fsm_state = SENS_IDLE;
unsigned long last_meas = 0;

uint16_t  scrollDelay;

#define CHAR_SPACING  1 /* pixels between characters */

/* Global message buffers shared by Serial and Scrolling functions */
#define BUF_SIZE  75
char curMessage[BUF_SIZE];
char newMessage[BUF_SIZE];
bool newMessageAvailable = false;

void readSerial(void)
{
	static uint8_t  putIndex = 0;

	while (Serial.available()) {
		newMessage[putIndex] = (char) Serial.read();
		if ((newMessage[putIndex] == '\n') || (putIndex >= BUF_SIZE-3)) {
			newMessage[putIndex++] = ' ';
			newMessage[putIndex] = '\0';
			putIndex = 0;
			newMessageAvailable = true;
		} else if (newMessage[putIndex] != '\r') {
			putIndex++;
		}
	}
}

void scrollDataSink(uint8_t dev, MD_MAX72XX::transformType_t t, uint8_t col)
{
#if PRINT_CALLBACK
  Serial.print("\n cb ");
  Serial.print(dev);
  Serial.print(' ');
  Serial.print(t);
  Serial.print(' ');
  Serial.println(col);
#endif
}

uint8_t scrollDataSource(uint8_t dev, MD_MAX72XX::transformType_t t)
{
	static char *p = curMessage;
	static uint8_t state = 0;
	static uint8_t curLen, showLen;
	static uint8_t cBuf[8];
	uint8_t colData;

	switch(state) {
		case 0:
			showLen = mx.getChar(*p++, sizeof(cBuf) / sizeof(cBuf[0]),
					cBuf);
			curLen = 0;
			state++;

			if (*p == '\0') {
				p = curMessage;
				if (newMessageAvailable) {
					char tmpstr[6];
					dtostrf(curTemp, 4, 1, tmpstr);
					sprintf(curMessage,
						"%s   /   TEMP    %s C   /   ",
						newMessage, tmpstr);
					newMessageAvailable = false;
				}
			}

		case 1:
			colData = cBuf[curLen++];
			if (curLen == showLen) {
				showLen = CHAR_SPACING;
				curLen = 0;
				state = 2;
			}
			break;
		case 2:
			colData = 0;
			curLen++;
			if (curLen == showLen)
				state = 0;
			break;
		default:
			state = 0;
	}

	return(colData);
}

void scrollText(void)
{
	static uint32_t prevTime = 0;

	if (millis()-prevTime >= scrollDelay) {
		mx.transform(MD_MAX72XX::TSL);
		prevTime = millis();
	}
}

uint16_t getScrollDelay(void)
{
#if USE_POT_CONTROL
	uint16_t  t;

	t = analogRead(SPEED_IN);
	t = map(t, 0, 1023, 25, 250);

	return(t);
#else
	return(SCROLL_DELAY);
#endif
}

int searchSensor()
{
	onewire.reset_search();
	while(onewire.search(sensorAddress)) {
		if (sensorAddress[0] != 0x28)
			continue;

		if (OneWire::crc8(sensorAddress, 7) != sensorAddress[7])
			return -1;
		return 0;
	}
	return 0;
}

void ds18b20_fsm(void)
{
	switch(fsm_state) {
	case SENS_IDLE:
		if (millis() > last_meas + DS18B20_POLL_PERIOD) {
			sensors.request(sensorAddress);
			fsm_state = SENS_REQ;
		}
		break;
	case SENS_REQ:
		if (sensors.available()) {
			curTemp = sensors.readTemperature(sensorAddress);
			last_meas = millis();
			fsm_state = SENS_IDLE;
		} else {
			if (millis() > last_meas + (4 * DS18B20_POLL_PERIOD)) {
				last_meas = millis();
				fsm_state = SENS_IDLE;
			}
		}
		break;
	default:
		fsm_state = SENS_IDLE;
		break;
	}
}

void setup()
{
	mx.begin();
	mx.setShiftDataInCallback(scrollDataSource);
	mx.setShiftDataOutCallback(scrollDataSink);

	searchSensor();
	sensors.begin();
	last_meas = millis();
	sensors.request(sensorAddress);
	fsm_state = SENS_REQ;

#if USE_POT_CONTROL
	pinMode(SPEED_IN, INPUT);
#else
	scrollDelay = SCROLL_DELAY;
#endif

	strcpy(curMessage, "    KALSTOCK    ");
	newMessage[0] = '\0';

	Serial.begin(57600);
	Serial.print("\n");
}

void loop()
{
	ds18b20_fsm();
	scrollDelay = getScrollDelay();

	readSerial();
	scrollText();
}
