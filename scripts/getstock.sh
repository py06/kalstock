#!/bin/sh

if [ $# -lt 1 ]
then
	echo "Usage: $0 <device>"
	echo "device: is the dev name of the serial port used to communicate with arduino"
	echo "Example: $0 /dev/ttyUSB0"
	exit 1
fi

URL="https://www.boursorama.com/cours/1rPALKAL/" #URL of the stock
CSSDIVCLSID="c-faceplate__info" #name of the css div class id for the stock price
if [ -e $1 ]
then
	DEV="$1"
else
	echo "$1 does not exist"
	exit 2
fi
STOCKNAME="ALKAL"

while true
do
	#WGET: -q => quiet
	#WGET: -O - => output to stdout
	data=`wget -q -O - ${URL} |grep ${CSSDIVCLSID}`
	price=`echo $data | sed -e "s/\(.*data-ist-last>\)\([0-9]*\.[0-9]*\)\(<\/span>.*\)/\2/"`
	#price=`cat fake_price.txt`
	price=`awk "BEGIN {print $price}" ` #remove trailing 0
	echo "$STOCKNAME    $price    " > $DEV
	sleep 120s
done

