#!/bin/sh

if [ $# -lt 1 ]
then
	echo "Usage: $0 [-r] <dev>"
	exit 1
fi

DEV=$1

stty -F $DEV -g > stty.bkup


stty -F $DEV cs8 57600 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts -hupcl
