#!/usr/bin/python3

import json
import urllib.request
import urllib.error
import sys
import serial
import glob
import time
import os.path

alkal_url="https://www.boursorama.com/bourse/action/graph/ws/UpdateCharts?symbol=1rPALKAL"
stockname="ALKAL"
stockprice = float('-inf')
stockstring = "N/A"

baud=57600

def fetch_stockprice(url, name):
    try:
        response = urllib.request.urlopen(url)
    except:
        print("Could not retrieve url %s" % url)
        return float('-inf')
    data = json.loads(response.read().decode("utf-8"))
    try:
        if type(data) is list:
            price = float(f"{data[0]['c']}")
        elif type(data) is dict:
            price = float(f"{data['c']}")
    except:
        price = float('-inf')

    return price


def fetch_stockstring(url, name):
    global stockstring
    global stockprice

    newprice = fetch_stockprice(url, name)
    if (newprice != float('-inf')):
        stockprice = newprice
        stockstring="{0:.2f}".format(newprice)

    disp_bytesstr=name + "     " + stockstring + "    \n"
    print("%s" % disp_bytesstr.encode('utf-8'))
    return disp_bytesstr.encode('utf-8')

def write_serial(ser, bytestr):
    if (not os.path.exists(ser.port)):
        print("%s disappeared" % ser.port)
        return -1
    try:
        ser.write(bytestr)
    except:
        print("issue with %s - %s" % (ser.port, str(err)))
        return -2
    return 0

def search_usb():
    try:
        port = glob.glob("/dev/ttyUSB*")[0]
    except IndexError as err:
        print("ttyUSB not found - %s" % str(err))
        port = ""
    return port

def open_usb(port):
    try:
        ser = serial.Serial(port=port, baudrate=baud, bytesize=8,
                parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE,
                xonxoff=False, rtscts=False, dsrdtr=False)
    except:
        print("Could not open %s" % port)
        return None

    return ser


def main_loop():
    state = 0

    while True:
        if (state == 0):
            port = search_usb()
            if (port):
                state += 1
            else:
                time.sleep(8)
        if (state == 1):
            ser = open_usb(port)
            time.sleep(2)
            if (not ser):
                ser.close()
                state = 0
            else:
                state += 1
        if (state == 2):
            stock = fetch_stockstring(alkal_url, stockname)
            res = write_serial(ser, stock)
            if (res < 0):
                ser.close()
                state = 0
            time.sleep(30)
    sys.exit(0)

if __name__ == "__main__":
    main_loop()
