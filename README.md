# Whats is kalstock #
kalstock is a project aimed at displaying kalray stock price on a led matrix. There are 2 parts:
- a script on host side fetching stock price and feeding it through serial port to arduino
- arduino code that gets stock price + sensor input and displays them on led matrix

# Dependencies #

## Arduino core ##
- Arduino
- Arduino-Makefile

## Arduino libraries ##
- SPI (arduino core lib)
- [MD_MAX72xx](https://github.com/MajicDesigns/MD_MAX72XX) : low level led matrix lib
- [MD_Parola](https://github.com/MajicDesigns/MD_Parola) : upper-levle library for led matrices
- [OneWire](https://github.com/PaulStoffregen/OneWire) : DS18B20 talk 1Wire
- [DS18B20](https://github.com/nettigo/DS18B20) : DS18B20 async library


# Notes #
This project is very simple. It is mainly a copy of some MD_Parola example with some added scripts to feed the expected data to be displayed
